from application import gamebook


def create_db(db):
    db.drop_all()
    db.create_all()

    db.session.commit()

if __name__ == "__main__":
    gamebook.app = gamebook.create_app()
    with gamebook.app.app_context():
        create_db(gamebook.app.db)

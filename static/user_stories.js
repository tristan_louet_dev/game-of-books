'use strict';

var UserStories = (function($) {
    //Templates variables so that we don't have to do it every time we use them
    var init = function init() {
        UserStories.storyTemplate = Handlebars.compile($("#story-template").html());
    };

    //Building sequence list and adding functions like delete on them
    function buildStoriesList() {
        $.get($("#stories-data").data("get-stories-url"), function(result) {
            if (result.result != 200) {
                alert("An error (code " + result.result + ") occured while attempting to retrieve stories");
            } else {
                $("#storiesList").html("");
                for (var storyIndex in result.data.stories) {
                    var story = result.data.stories[storyIndex];
                    console.log(story);
                    $("#storiesList").append(UserStories.storyTemplate({'story': story}));
                }

                $(".story-deleter").click(function(event) {
                    $.post($(this).data("deleter-url"), function (result) {
                        if (result.result != 200) {
                            alert("An error (code " + result.result + ") occured while attempting to delete sequence.");
                        }
                        buildStoriesList();
                    });
                    event.preventDefault();
                });

                $(".story-editor").click(function (event) {
                    window.location.href = $(this).data("editor-url");
                    event.preventDefault();
                });

                $(".story-player").click(function (event) {
                    window.location.href = $(this).data("player-url")
                    event.preventDefault();
                });
            }
        });
    };

    return {
        'init': init,
        'buildStoriesList': buildStoriesList
    };
})(jQuery);

$(document).ready(function() {
    UserStories.init();
});

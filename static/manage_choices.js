'use strict';

var ChoicesEditor = (function($) {
    //Templates variables so that we don't have to do it every time we use them
    var init = function init() {
        ChoicesEditor.choiceTemplate = Handlebars.compile($("#choice-template").html());

        $("#addChoiceForm").submit(choiceFormAction($("#sequence-data").data("create-choice-url")));
    };

    function choiceFormAction(url) {
        return function(event) {
            if ($("#successRate").val() < 0 || $("#successRate").val() > 100) {
                alert("The success rate must be between 0 and 100%");
                event.preventDefault();
                return;
            }
            $.post(url, {
                'success_sequence_id': $("#successSequenceId").val(),
                'failure_sequence_id': $("#failureSequenceId").val(),
                'success_rate': $("#successRate").val(),
                'description': $("#choiceDescription").val()
            }).done(function (result) {
                if (result.result != 200) {
                    if (result.result == 400) {
                        alert("Bad request: check the form inputs");
                    } else {
                        console.log(result);
                        alert("An error (code " + result.result + ") occured while attempting to create a new choice");
                    }
                } else {
                    buildChoicesList();
                    $("#form-cancel").trigger("click"); //We simulate a click to reinitialize the forms.
                }
            });
            event.preventDefault();
        }
    }

    //Building sequence list and adding functions like delete on them
    function buildChoicesList() {
        $.get($("#sequence-data").data("get-choices-url"), function(result) {
            if (result.result != 200) {
                console.log(result);
                alert("An error (code " + result.result + ") occured while attempting to retrieve sequence's choices");
            } else {
                $("#choicesList").html("");
                for (var choiceIndex in result.data.choices) {
                    var choice = result.data.choices[choiceIndex];
                    $("#choicesList").append(ChoicesEditor.choiceTemplate({'choice': choice}));
                }

                $(".delete-choice-button").click(function(event) {
                    $.post($(this).data("delete-url"), function (result) {
                        if (result.result != 200) {
                            console.log(result);
                            alert("An error (code " + result.result + ") occured while attempting to delete a choice");
                        } else {
                            $("#form-cancel").trigger("click"); //We simulate a click to reinitialize the forms.
                            buildChoicesList();
                        }
                    });
                    event.preventDefault();
                });

                $("#form-cancel").click(function (event) {
                    $("#addChoiceForm").unbind("submit");
                    $("#addChoiceForm").submit(choiceFormAction($("#sequence-data").data("create-choice-url")));
                    $("#form-submit").html('<span class="glyphicon glyphicon-plus"></span> Create choice');
                    $('#successSequenceId option[value="-1"]').prop('selected', true);
                    $('#failureSequenceId option[value="-1"]').prop('selected', true);
                    $("#choiceDescription").val("");
                    $("#successRate").val("100");
                    $("#form-cancel-span").prop("hidden", true);

                    event.preventDefault();
                });

                $(".edit-choice-button").click(function(event) {
                    $("#addChoiceForm").unbind("submit");
                    $("#addChoiceForm").submit(choiceFormAction($(this).data("edit_url")));
                    $("#choiceDescription").val($(this).data("description-value"));
                    $("#successRate").val($(this).data("success-rate-value"));
                    $("#form-cancel-span").prop("hidden", false);
                    $('#successSequenceId option[value="' + $(this).data("success-sequence-id") + '"]').prop('selected', true);
                    $('#failureSequenceId option[value="' + $(this).data("failure-sequence-id") + '"]').prop('selected', true);
                    $("#form-submit").html('<span class="glyphicon glyphicon-edit"></span> Edit choice');

                    event.preventDefault();
                });
            }
        });
    };

    return {
        'init': init,
        'buildChoicesList': buildChoicesList
    };
})(jQuery);

$(document).ready(function() {
    ChoicesEditor.init();
});

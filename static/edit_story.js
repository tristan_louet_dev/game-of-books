'use strict';

var StoryEditor = (function($) {
    //Templates variables so that we don't have to do it every time we use them
    var init = function init() {
        StoryEditor.sequenceTemplate = Handlebars.compile($("#sequence-template").html());

        $("#addSequenceButton").click(function(event) {
            saveAllSequences();
            $.post($("#story-data").data("create-sequence-url"), function(result) {
                if (result.result != 200) {
                    alert("An error (code " + result.result + ") occured while attempting to create a new sequence");
                } else {
                    buildSequencesList();
                }
            });
        });
    };

    //Building sequence list and adding functions like delete on them
    function buildSequencesList() {
        $.get($("#story-data").data("get-sequences-url"), function(result) {
            if (result.result != 200) {
                alert("An error (code " + result.result + ") occured while attempting to retrieve story's sequences");
            } else {
                $("#sequencesList").html("");
                for (var seqIndex in result.data.sequences) {
                    var seq = result.data.sequences[seqIndex];
                    $("#sequencesList").append(StoryEditor.sequenceTemplate({'sequence': seq}));
                }

                $(".sequence-deleter").click(function(event) {
                    $.post($(this).data("delete-sequence-url"), function (result) {
                        if (result.result != 200) {
                            alert("An error (code " + result.result + ") occured while attempting to delete sequence.");
                        }
                        buildSequencesList();
                    });
                    event.preventDefault();
                });

                $(".choices-button").click(function(event) {
                    saveAllSequences();
                    window.location.href = $(this).data("choices-url");
                    event.preventDefault();
                });

                $(".sequence-item").click(function(event) {
                    var editorDiv = $("#" + $(this).data("editor-id"));
                    var oldProp = editorDiv.prop("hidden");
                    $(".sequence-editor").prop("hidden", true);
                    $(".sequence-editor").parent().removeClass("active");
                    editorDiv.prop("hidden", !oldProp);
                    if (editorDiv.prop("hidden")) {
                        editorDiv.parent().removeClass("active");
                    } else {
                        editorDiv.parent().addClass("active");
                    }
                    editorDiv.children("form").submit(function(event) {
                        saveSequenceItem(editorDiv);
                        buildSequencesList();
                        event.preventDefault();
                    });
                });

                $("#saveAllSequences").click(function(event) {
                    saveAllSequences();
                    event.preventDefault();
                });
            }
        });
    };

    function saveAllSequences() {
        $(".sequence-editor").each(function() {
            saveSequenceItem($(this));
        });
        buildSequencesList();
    }

    function saveSequenceItem(sequenceEditorDiv) {
        var infoButton = $("#sequence" + sequenceEditorDiv.data("sequence-id") + "Info");
        infoButton.attr("class", "glyphicon glyphicon-refresh")
        $.post(sequenceEditorDiv.data("edit-url"), {
            'sequence_title': $("#sequence" + sequenceEditorDiv.data("sequence-id") + "Title").val(),
            'sequence_text': $("#sequence" + sequenceEditorDiv.data("sequence-id") + "Text").val()
        }).done(function (result) {
            if (result.result == 200) {
                infoButton.attr("class", "glyphicon glyphicon-ok");
            } else {
                console.log(result);
                infoButton.attr("class", "glyphicon glyphicon-remove");
            }
        });
    }

    return {
        'init': init,
        'buildSequenceList': buildSequencesList
    };
})(jQuery);

$(document).ready(function() {
    StoryEditor.init();
});

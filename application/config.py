# Configuration values.
SQLALCHEMY_DATABASE_URI = 'sqlite:///gamebook.db'
DEBUG = False
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'default'
DEBUG_TB_INTERCEPT_REDIRECTS = False
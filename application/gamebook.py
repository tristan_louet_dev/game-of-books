from flask import Flask
from flask.ext.login import LoginManager
from flask.ext.sqlalchemy import SQLAlchemy
# from flask_debugtoolbar import DebugToolbarExtension

import sys
reload(sys)
sys.setdefaultencoding("utf-8")

app = None
db = SQLAlchemy()
login_manager = None

def create_app():
    global app
    app = Flask(__name__, template_folder='../templates', static_folder='../static')
    app.config.from_object('application.config')

    global db
    db.init_app(app)
    app.db = db

    global login_manager
    login_manager = LoginManager()
    login_manager.init_app(app)

    from controllers import views

    from models import models

    # DebugToolbarExtension(app)

    return app

from flask import render_template, request, redirect, url_for, flash, jsonify, abort
from flask.ext.login import login_user, logout_user, current_user
from flask.ext.login import login_user
from sqlalchemy.orm import *
from application import gamebook
from models import models
from random import seed, randint

seed()

@gamebook.app.route('/')
def index():
    return render_template('index.html')


@gamebook.app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'GET':
        return render_template('register.html')
    # Method is POST
    if request.form['password'] != request.form['retyping']:
        flash("The password and its retyping didn't match")
        return render_template('register.html', pseudo=request.form['pseudo'])
    user = models.User(request.form['pseudo'], request.form['password'])
    if user.exists_in_database():
        flash("A user with that pseudo already exists")
        return render_template('register.html', pseudo=request.form['pseudo'])
    gamebook.db.session.add(user)
    gamebook.db.session.commit()
    flash("User " + user.pseudo + " registered and logged in. Welcome!")
    login_user(user, True)
    return redirect(url_for('index'))


@gamebook.app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@gamebook.app.route('/login', methods=['POST'])
def login():
    user = models.User.query.get(request.form['pseudo'])
    if user is None:
        flash("Wrong pseudo or password!")
    else:
        if user.password_is(request.form['password']):
            login_user(user, True)
        else:
            flash("Wrong pseudo or password!")
    return redirect(request.form['origin'])


@gamebook.app.route('/user/<string:user_id>/stories')
def user_stories(user_id):
    user = models.User.query.get_or_404(user_id)
    return render_template("user_stories.html", user=user)


@gamebook.app.route('/story/new', methods=['GET', 'POST'])
def new_story():
    if request.method == "GET":
        return render_template("new_story.html")
    # Method is post
    if not current_user.is_authenticated():
        flash("You must be logged in to create a story")
        return redirect(url_for('index'))
    story = models.Story(request.form['title'], current_user.get_id(), "")
    gamebook.db.session.add(story)
    gamebook.db.session.commit()
    return redirect(url_for('edit_story', story_id=story.id))


@gamebook.app.route('/story/<int:story_id>/edit', methods=['GET', 'POST'])
def edit_story(story_id):
    story = models.Story.query.get_or_404(story_id)
    if not current_user.is_authenticated() or current_user.get_id() != story.user_id:
        abort(401) # Unauthorized
    # OK
    if request.method == "POST":
        story.title = request.form['title']
        story.summary = request.form['summary']
        gamebook.db.session.commit()
        flash("Changes saved")
    return render_template("edit_story.html", story=story)


@gamebook.app.route('/sequence/<int:sequence_id>/choices')
def sequence_choices(sequence_id):
    seq = models.Sequence.query.get_or_404(sequence_id)
    if not current_user.is_authenticated() or current_user.get_id() != seq.story.user_id:
        abort(401)
    # OK
    return render_template("manage_story_choices.html", sequence=seq)


@gamebook.app.route('/gallery/<int:page>')
def stories_gallery(page):
    stories = models.Story.query.paginate(page, per_page=10)
    return render_template("stories_gallery.html", storiesPagination=stories)


@gamebook.app.route('/story/<string:story_id>/play')
def play_story(story_id):
    story = models.Story.query.get_or_404(story_id)
    if story.sequences.count() <= 0:
        return render_template("end_of_story.html", story=story)
    return render_template("play_story.html", story=story)


@gamebook.app.route('/adventure/<int:sequence_id>/')
def display_sequence(sequence_id):
    sequence = models.Sequence.query.get_or_404(sequence_id)
    return render_template("display_sequence.html", sequence=sequence)


@gamebook.app.route('/adventure/choice/<int:choice_id>/process')
def process_choice(choice_id):
    choice = models.Choice.query.get_or_404(choice_id)
    next_sequence_id = choice.failure_sequence_id
    if next_sequence_id == -1 or randint(0, 100) <= choice.success_rate:
        next_sequence_id = choice.success_sequence_id
    return redirect(url_for("display_sequence", sequence_id=next_sequence_id))


@gamebook.app.route('/adventure/<int:story_id>/theEnd')
def end_story(story_id):
    story = models.Story.query.get_or_404(story_id)
    return render_template("end_of_story.html", story=story)


# API
# A real API would offer some way to authenticate via the API, but her we'll just
# assume an API user has been authenticated via the website.

# We should paginate a bit of every thing in real life, but for the API let's make our lifes easier
# since it's not a production server
@gamebook.app.route('/api/user/<string:user_id>/stories/get', methods=['GET'])
def get_user_stories(user_id):
    user = models.User.query.get(user_id)
    if user is None:
        return api_make_result(404, {})
    # User exists
    stories = models.Story.query.filter_by(user_id=user_id).all()
    res_list = [story.get_jsonable_dict() for story in stories]
    for storyDict in res_list:
        storyDict['editor_url'] = url_for('edit_story', story_id=storyDict['id'])
        storyDict['deleter_url'] = url_for('delete_story', story_id=storyDict['id'])
        storyDict['play_url'] = url_for('play_story', story_id=storyDict['id'])
        storyDict['pages_nb'] = models.Story.query.get(storyDict['id']).sequences.count()

    return api_make_result(200, {'stories': res_list})

@gamebook.app.route('/api/user/<string:user_id>/exists', methods=['GET'])
def user_exists_api(user_id):
    user = models.User(user_id, "")
    return api_make_result(200, {'user_exists': user.exists_in_database()})


@gamebook.app.route('/api/story/<string:story_id>/delete', methods=['POST'])
def delete_story(story_id):
    story = models.Story.query.get(story_id)
    if story is None:
        return api_make_result(404, {})
    # Story exists
    if not current_user.is_authenticated() or current_user.get_id() != story.user_id:
        return api_make_result(401, {})
    gamebook.db.session.delete(story)
    gamebook.db.session.commit()
    return api_make_result(200, {})


@gamebook.app.route('/api/story/<string:story_id>/sequences/create', methods=['POST'])
def story_create_sequence(story_id):
    story = models.Story.query.get(story_id)
    if story is None:
        return api_make_result(404, {})
    # Story exists
    if not current_user.is_authenticated() or current_user.get_id() != story.user_id:
        return api_make_result(401, {})
    seq = models.Sequence("Sequence", story.id)
    gamebook.db.session.add(seq)
    gamebook.db.session.commit()
    return api_make_result(200, {'sequence': seq.id})


@gamebook.app.route('/api/story/<string:story_id>/sequences/get', methods=['GET'])
def get_story_sequences(story_id):
    story = models.Story.query.get(story_id)
    if story is None:
        return api_make_result(404, {})
    # Story exists
    seqs = models.Sequence.query.filter_by(story_id=story_id)
    res_list = [seq.get_jsonable_dict() for seq in seqs.all()]
    for seqDict in res_list:
        seqDict['delete_url'] = url_for('delete_sequence', sequence_id=seqDict['id'])
        seqDict['edit_url'] = url_for('edit_sequence', sequence_id=seqDict['id'])
        seqDict['manage_choices_url'] = url_for('sequence_choices', sequence_id=seqDict['id'])
        seqDict['choices_number'] = models.Choice.query.filter_by(sequence_id=seqDict['id']).count()
    return api_make_result(200, {'sequences': res_list})


@gamebook.app.route('/api/sequence/<int:sequence_id>/update', methods=['POST'])
def edit_sequence(sequence_id):
    seq = models.Sequence.query.get(sequence_id)
    if seq is None:
        return api_make_result(404, {})
    # Sequence exists
    if not current_user.is_authenticated() or current_user.get_id() != seq.story.user_id:
        return api_make_result(401, {})
    seq.title = request.form['sequence_title']
    seq.text = request.form['sequence_text']
    gamebook.db.session.commit()
    return api_make_result(200, {})


@gamebook.app.route('/api/sequence/<int:sequence_id>/delete', methods=['POST'])
def delete_sequence(sequence_id):
    sequence = models.Sequence.query.get(sequence_id)
    if sequence is None:
        return api_make_result(404, {})
    # Sequence exists
    if not current_user.is_authenticated() or current_user.get_id() != sequence.story.user_id:
        return api_make_result(401, {})
    gamebook.db.session.delete(sequence)
    gamebook.db.session.commit()
    return api_make_result(200, {})



@gamebook.app.route('/api/sequence/<int:sequence_id>/choices/get', methods=['GET'])
def sequence_get_choices(sequence_id):
    sequence = models.Sequence.query.get(sequence_id)
    if sequence is None:
        return api_make_result(404, {})
    # Sequence exists
    choices = models.Choice.query.filter_by(sequence_id=sequence_id)
    res_list = [choice.get_jsonable_dict() for choice in choices.all()]
    for choiceDict in res_list:
        choiceDict['delete_url'] = url_for('delete_choice', choice_id=choiceDict['id'])
        choiceDict['edit_url'] = url_for('edit_choice', choice_id=choiceDict['id'])
    return api_make_result(200, {'choices': res_list})


@gamebook.app.route('/api/choice/<int:choice_id>/update', methods=['POST'])
def edit_choice(choice_id):
    choice = models.Choice.query.get(choice_id)
    if choice is None:
        return api_make_result(404, {})
    # Choice exists
    if not current_user.is_authenticated() or current_user.get_id() != choice.sequence.story.user_id:
        return api_make_result(401, {})
    # OK
    success_seq_id = int(request.form['success_sequence_id'])
    if success_seq_id != -1:
        success_seq = models.Sequence.query.get(request.form['success_sequence_id'])
        if success_seq is None:
            return api_make_result(400, {'message': 'sequence_id ' + success_seq_id + ' unknown'})

    failure_seq_id = int(request.form['failure_sequence_id'])
    if failure_seq_id != -1:
        failure_seq = models.Sequence.query.get(request.form['failure_sequence_id'])
        if failure_seq is None:
            return api_make_result(400, {'message': 'sequence_id ' + failure_seq_id + ' unknown'})

    success_rate = int(request.form['success_rate'])
    if success_rate < 0:
        return api_make_result(400, {'message': "success_rate cannot be lesser than 0"})
    if success_rate > 100:
        return api_make_result(400, {'message': "success_rate cannot be greater than 100"})

    # Everything is good
    choice.description = request.form['description']
    choice.success_sequence_id = success_seq_id
    choice.failure_sequence_id = failure_seq_id
    choice.success_rate = success_rate
    gamebook.db.session.commit()
    return api_make_result(200, {})


@gamebook.app.route('/api/choice/<int:choice_id>/delete', methods=['POST'])
def delete_choice(choice_id):
    choice = models.Choice.query.get(choice_id)
    if choice is None:
        return api_make_result(404, {})
    # Choice exists
    if not current_user.is_authenticated() or current_user.get_id() != choice.sequence.story.user_id:
        return api_make_result(401, {})
    # OK
    gamebook.db.session.delete(choice)
    gamebook.db.session.commit()
    return api_make_result(200, {})


@gamebook.app.route('/api/sequence/<int:sequence_id>/choices/create', methods=['POST'])
def create_choice(sequence_id):
    sequence = models.Sequence.query.get(sequence_id)
    if sequence is None:
        return api_make_result(404, {})
    # Sequence exists
    if not current_user.is_authenticated() or current_user.get_id() != sequence.story.user_id:
        return api_make_result(401, {})
    # OK
    success_seq_id = int(request.form['success_sequence_id'])
    if success_seq_id != -1:
        success_seq = models.Sequence.query.get(request.form['success_sequence_id'])
        if success_seq is None:
            return api_make_result(400, {'message': 'sequence_id ' + success_seq_id + ' unknown'})

    failure_seq_id = int(request.form['failure_sequence_id'])
    if failure_seq_id != -1:
        failure_seq = models.Sequence.query.get(request.form['failure_sequence_id'])
        if failure_seq is None:
            return api_make_result(400, {'message': 'sequence_id ' + failure_seq_id + ' unknown'})

    success_rate = int(request.form['success_rate'])
    if success_rate < 0:
        return api_make_result(400, {'message': "success_rate cannot be lesser than 0"})
    if success_rate > 100:
        return api_make_result(400, {'message': "success_rate cannot be greater than 100"})

    # Everything is good
    choice = models.Choice(sequence_id, request.form['description'], success_seq_id, failure_seq_id, success_rate)
    gamebook.db.session.add(choice)
    gamebook.db.session.commit()
    return api_make_result(200, {})


def api_make_result(http_response_code, data):
    return jsonify({'result': http_response_code, 'data': data})

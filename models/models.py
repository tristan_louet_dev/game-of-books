from application import gamebook
import hmac

db = gamebook.app.db
super_secret_hmac_key = '8N$|Dq`aN7`^2!t"3MGzE3rLza<Sd$'


class User(db.Model):
    pseudo = db.Column(db.String(128), primary_key=True)
    password = db.Column(db.String(56))
    stories = db.relationship('Story', backref='user', lazy='dynamic')

    def __init__(self, pseudo, clear_password):
        self.pseudo = pseudo
        digester = hmac.new(super_secret_hmac_key)
        digester.update(clear_password)
        self.password = digester.hexdigest()

    def password_is(self, password):
        digester = hmac.new(super_secret_hmac_key)
        digester.update(password)
        return digester.hexdigest() == self.password

    def exists_in_database(self):
        user = User.query.get(self.pseudo)
        return user is not None

    def is_authenticated(self):
        return True

    def is_anonymous(self):
        return False

    def is_active(self):
        return True

    def get_id(self):
        return self.pseudo


@gamebook.login_manager.user_loader
def user_loader(userid):
    return User.query.get(userid)


class Story(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(56))
    summary = db.Column(db.Text)
    user_id = db.Column(db.String(128), db.ForeignKey('user.pseudo'))
    sequences = db.relationship('Sequence', backref='story', lazy='dynamic')

    def __init__(self, title, user_id, summary):
        self.title = title
        self.user_id = user_id
        self.summary = summary

    def get_jsonable_dict(self):
        return {'id': self.id, 'title': self.title, 'summary': self.summary, 'user_id': self.user_id,
                'sequences': [seq.get_jsonable_dict() for seq in self.sequences]}


class Choice(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.Text)
    sequence_id = db.Column(db.Integer, db.ForeignKey('sequence.id'))
    success_rate = db.Column(db.Integer)
    success_sequence_id = db.Column(db.Integer, db.ForeignKey('sequence.id'))
    failure_sequence_id = db.Column(db.Integer, db.ForeignKey('sequence.id'))

    def __init__(self, sequence_id, description, success_sequence_id, failure_sequence_id, success_rate):
        self.sequence_id = sequence_id
        self.description = description
        self.success_sequence_id = success_sequence_id
        self.failure_sequence_id = failure_sequence_id
        self.success_rate = success_rate

    def get_jsonable_dict(self):
        return {'id': self.id, 'description': self.description, 'sequence_id': self.sequence_id,
                'success_rate': self.success_rate, 'success_sequence_id': self.success_sequence_id,
                'failure_sequence_id': self.failure_sequence_id}


class Sequence(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(256))
    text = db.Column(db.Text)
    story_id = db.Column(db.Integer, db.ForeignKey('story.id'))
    choices = db.relationship('Choice', backref='sequence', lazy='dynamic', foreign_keys=Choice.sequence_id)

    def __init__(self, title, story_id):
        self.title = title
        self.story_id = story_id
        self.text = ""

    def get_jsonable_dict(self):
        return {'id': self.id, 'title': self.title, 'story_id': self.story_id, 'text': self.text,
                'choices': [choice.get_jsonable_dict() for choice in self.choices]}

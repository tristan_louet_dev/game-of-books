from application import gamebook

if __name__ == '__main__':
    app = gamebook.create_app()
    app.run()
